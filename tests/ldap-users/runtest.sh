#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/cronie/Regression/ldap-users
#   Description: Test for cronie not creating jobs for ldap users if ldap
#   Author: Jakub Prokes <jprokes@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="cronie"

rlJournalStart
    rlPhaseStart FAIL "General Setup"
        rlAssertRpm $PACKAGE
        rlRun "tmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "testDir=\"$(pwd)\""
        rlRun "pushd $tmpDir"
        rlRun "rlImport --all"
        rlServiceStart "crond"
        rlFileBackup /var/log/cron
        echo > /var/log/cron
        /bin/kill -HUP $(cat /var/run/syslogd.pid)
        rlRun "getent passwd user1" 2
    rlPhaseEnd

    rlPhaseStart FAIL "LDAP Server Setup"
        rlServiceStop "slapd"
        rlFileBackup --clean "/etc/openldap"
        rlFileBackup --clean "/var/run/openldap"
        rlFileBackup --clean "/var/lib/ldap"
        rlRun "rm -rf /etc/openldap/slapd.d/* /var/lib/ldap/* /var/run/openldap/*" 0 "Cleaning LDAP directories"
        rlRun "slapadd -l ${testDir}/data.ldif -f ${testDir}/slapd.conf" 0 "Importing testing user into LDAP"
        rlRun "chown ldap:ldap /var/run/openldap/*" 0 "Fixing permissions on '/var/run/openldap/*'"
        rlRun "restorecon -Rv /etc/" 0 "Fixing SELinux contexts"
        rlRun "slaptest -f ${testDir}/slapd.conf -F /etc/openldap/slapd.d" 0 "Importing LDAP configuration"
        rlRun "chown -R ldap:ldap /etc/openldap/slapd.d" 0 "Fixing permissions on '/etc/openldap/slapd.d'"
        rlServiceStart "slapd"
    rlPhaseEnd

    rlPhaseStart FAIL "LDAP auth setup"
        rlServiceStop "sssd"
        rlRun "authconfig_setup"
        rlRun "authconfig --savebackup=CoreOS_cronie_Regression_ldap-users"
        rlRun "authconfig --enableldap --enableldapauth --ldapserver=ldap://127.0.0.1/ \
            --ldapbasedn=dc=foo,dc=bar,dc=com --update"
        rlRun "getent passwd user1"
        rlRun "mkdir -p /home/ldap/user1"
        rlRun "chown user1 /home/ldap/user1"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "su user1 -c 'crontab ${testDir}/user1.cron'" 0 "Creating user cronjob" && \
        cat ${testDir}/user1.cron;
        rlFileBackup /etc/crontab
        rlRun "rlServiceStop slapd"
        rlRun "service sssd stop"
        rlRun "rm -rf /var/lib/sss/db/*"
        rlRun "service sssd start"
        rlRun "getent passwd user1" 2
        rlRun "echo \"*  *  *  *  * user1 /bin/echo foo > /tmp/cron.out\" > /etc/crontab" 0 \
            "Create record in system crontab"
        cat /etc/crontab
        rlRun "service crond restart"
        rlRun "rlServiceStart slapd"
        rlRun "service sssd stop"
        rlRun "rm -rf /var/lib/sss/db/*"
        rlRun "service sssd start"
        waitCounter=60;
        echo "Waiting for LDAP"
        while ! getent passwd user1; do
            sleep 1;
            echo -n .
            [[ waitCounter -le 0 ]] && break;
            waitCounter=$((waitCounter-1));
        done
        echo;
        rlRun "getent passwd user1"
        rm -f /home/ldap/user1/cron.out
        echo "Waiting for cronjob execution"
        sleep 65;
        if rlRun "[[ -f /home/ldap/user1/cron.out ]]" 0 "User cronjob executed successfully"; then
                rlAssertGrep "foo" /home/ldap/user1/cron.out;
        fi
        if rlRun "[[ -f /tmp/cron.out ]]" 0 "Crontab cronjob executed successfully"; then
                rlAssertGrep "foo" /tmp/cron.out;
        fi
        rlRun "service crond stop"
        cat /var/log/cron
    rlPhaseEnd

    rlPhaseStart WARN "Cleanup"
        rlRun "service crond restart"
        rlRun "crontab -r -u user1"
        rlRun "rlServiceStop slapd"
        rlRun "authconfig --disableldap --disableldapauth --update"
        rlRun "authconfig --restorebackup=CoreOS_cronie_Regression_ldap-users"
        rlRun "authconfig_cleanup"
        rlRun "popd"
        rlRun "rm -r $tmpDir" 0 "Removing tmp directory"
        rlRun "rm -r /home/ldap/user1"
        rlRun "rm -r /home/ldap"
        rlFileRestore
        rlServiceRestore slapd
        rlServiceRestore sssd
        /bin/kill -HUP $(cat /var/run/syslogd.pid)
        rlServiceRestore crond
        #avoid systemd failing to start crond due start-limit
        sleep 10
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
