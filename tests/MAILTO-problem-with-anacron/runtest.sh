#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/cronie/Regression/MAILTO-problem-with-anacron
#   Description: Test for MAILTO problem with anacron
#   Author: Karel Volny <kvolny@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="cronie"
TestDir=$PWD

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlRun "useradd testuser1" 0 "Adding the testing user"
        rlFileBackup --clean /var/spool/mail/root /var/log/cron
        # stop cron not to interfere with our logs
        rlServiceStop crond
    rlPhaseEnd

    # email should be sent to user specified via MAILTO (testuser1)
    rlPhaseStartTest anacronmail
        rlRun "truncate --size 0 /var/spool/mail/testuser1" 0 "Truncating mail queue for the user 'testuser1'"
        rlRun "truncate --size 0 /var/log/cron" 0 "Truncating cron log"
        # DEBUG
            echo "anacrontab:"
            echo "***********"
            cat ${TestDir}/anacrontab
        rlRun "anacron -f -n -d -t ${TestDir}/anacrontab" 0 "Forcing anacron to run job"
        sleep 5
        # DEBUG
            echo "/var/spool/mail/testuser1:"
            echo "**************************"
            cat /var/spool/mail/testuser1
        rlAssertGrep "anacron" "/var/spool/mail/testuser1"
        rlAssertGrep "To: testuser1" "/var/spool/mail/testuser1"
        # DEBUG
            echo "/var/log/cron:"
            echo "**************"
            cat /var/log/cron
        rlAssertGrep "\(produced output\)" /var/log/cron
        rlAssertNotGrep "not mailing" /var/log/cron -i
    rlPhaseEnd

    # email should NOT be sent with empty MAILTO
    rlPhaseStartTest noanacronmail-empty
        rlRun "truncate --size 0 /var/spool/mail/root" 0 "Truncating mail queue for root"
        rlRun "truncate --size 0 /var/log/cron" 0 "Truncating cron log"
        rlRun "sed -i -e 's/MAILTO=testuser1/MAILTO=/' ${TestDir}/anacrontab" 0 "Removing mailto user from anacrontab"
        # DEBUG
            echo "anacrontab:"
            echo "***********"
            cat ${TestDir}/anacrontab
        rlRun "anacron -f -n -d -t ${TestDir}/anacrontab" 0 "Forcing anacron to run job"
        sleep 5
        # DEBUG
            echo "/var/spool/mail/root:"
            echo "*********************"
            cat /var/spool/mail/root
        rlAssertNotGrep "anacron" "/var/spool/mail/root"
        # DEBUG
            echo "/var/log/cron:"
            echo "**************"
            cat /var/log/cron
        rlAssertGrep "\(produced output\)" /var/log/cron
        rlAssertGrep "not mailing" /var/log/cron -i
    rlPhaseEnd

    # email should be sent to nonexisting user double-doublequotes, as the string after '=' is treated literally
    rlPhaseStartTest anacronmail-quotes
        rlRun "truncate --size 0 /var/spool/mail/root" 0 "Truncating mail queue for root"
        rlRun "truncate --size 0 /var/log/cron" 0 "Truncating cron log"
        rlRun "sed -i -e 's/MAILTO=/MAILTO=\"\"/' ${TestDir}/anacrontab" 0 "Setting mailto to double quotes in anacrontab"
        # DEBUG
            echo "anacrontab:"
            echo "***********"
            cat ${TestDir}/anacrontab
        rlRun "anacron -f -n -d -t ${TestDir}/anacrontab" 0 "Forcing anacron to run job"
        sleep 5
        # DEBUG
            echo "/var/spool/mail/root:"
            echo "*********************"
            cat /var/spool/mail/root
        rlAssertGrep "anacron" "/var/spool/mail/root"
        #rlAssertGrep "To: \"\"" "/var/spool/mail/root"
        # ^ this doesn't work in Beaker for some reason, check the quotes in 'Received: ... for "";'
        rlAssertGrep "for \"\"" "/var/spool/mail/root"
        # DEBUG
            echo "/var/log/cron:"
            echo "**************"
            cat /var/log/cron
        rlAssertGrep "\(produced output\)" /var/log/cron
        rlAssertNotGrep "not mailing" /var/log/cron -i
    rlPhaseEnd

    # email should be sent to the user running anacron, when MAILTO is missing
    rlPhaseStartTest anacronmail-missing
        rlRun "truncate --size 0 /var/spool/mail/root" 0 "Truncating mail queue for root"
        rlRun "truncate --size 0 /var/log/cron" 0 "Truncating cron log"
        rlRun "sed -i -e '/MAILTO/d' ${TestDir}/anacrontab" 0 "Removing mailto from anacrontab"
        # DEBUG
            echo "anacrontab:"
            echo "***********"
            cat ${TestDir}/anacrontab
        rlRun "anacron -f -n -d -t ${TestDir}/anacrontab" 0 "Forcing anacron to run job"
        sleep 5
        # DEBUG
            echo "/var/spool/mail/root:"
            echo "*********************"
            cat /var/spool/mail/root
        rlAssertGrep "anacron" "/var/spool/mail/root"
        # DEBUG
            echo "/var/log/cron:"
            echo "**************"
            cat /var/log/cron
        rlAssertGrep "\(produced output\)" /var/log/cron
        rlAssertNotGrep "not mailing" /var/log/cron -i
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "userdel testuser1"
        rlServiceRestore crond
        rlFileRestore
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
        #avoid systemd failing to start crond due start-limit
        sleep 10
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
